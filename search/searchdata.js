var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvwxy",
  1: "bceimrt",
  2: "l",
  3: "acegkmprstv",
  4: "_cdefghorstu",
  5: "abcdefiklmnoprstvwxy",
  6: "bmrtv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

