var searchData=
[
  ['generic_5fclosedloop_2epy_66',['generic_closedloop.py',['../generic__closedloop_8py.html',1,'']]],
  ['get_5fdelta_67',['get_delta',['../classencoderDriver_1_1EncoderDriver.html#a95f13d24470c3ee417cbfd6f8b766c19',1,'encoderDriver::EncoderDriver']]],
  ['get_5fold_5fposition_68',['get_old_position',['../classencoderDriver_1_1EncoderDriver.html#addb77910befbf1d28e371dca1bb718c6',1,'encoderDriver::EncoderDriver']]],
  ['get_5fpos_69',['get_pos',['../classtouch__driver_1_1touch__driver.html#a1c2deca49d77d15faa574d1419f39ad4',1,'touch_driver::touch_driver']]],
  ['get_5fposition_70',['get_position',['../classencoderDriver_1_1EncoderDriver.html#a18649b85eeb0c7e248c03d2eea8ca20e',1,'encoderDriver::EncoderDriver']]],
  ['get_5fx_71',['get_x',['../classtouch__driver_1_1touch__driver.html#ab9a71107b337cdf182112b3bacecbed0',1,'touch_driver::touch_driver']]],
  ['get_5fy_72',['get_y',['../classtouch__driver_1_1touch__driver.html#a709865bab0c762072ba08f4e8c3e3948',1,'touch_driver::touch_driver']]],
  ['getchange_73',['getChange',['../change__getter_8py.html#ad2e216de4eb1809cab6bed03c0a6a41e',1,'change_getter']]]
];
