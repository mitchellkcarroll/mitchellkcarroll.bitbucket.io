var searchData=
[
  ['data_39',['data',['../ADC__backend_8py.html#a6c4d4b96d08792a54c1fad3d294ea10e',1,'ADC_backend.data()'],['../ADC__frontend_8py.html#a8e4960651166e12d5a55d68a76520aa9',1,'ADC_frontend.data()'],['../plotter_8py.html#aea96a072074d3a8f35f1678ebb176518',1,'plotter.data()'],['../temp__getter_8py.html#af44c7dde2a0133083890697b051fb003',1,'temp_getter.data()']]],
  ['debug_40',['debug',['../classencoderDriver_1_1EncoderDriver.html#ab8292ea898e8e1d41a2e15821ad7d329',1,'encoderDriver.EncoderDriver.debug()'],['../classmotorDriver_1_1MotorDriver.html#ac5485c210ff506c50cff0d308279eb03',1,'motorDriver.MotorDriver.debug()']]],
  ['delay_41',['delay',['../reaction_8py.html#ae903f82826c3b390c922b1f089304cab',1,'reaction.delay()'],['../reaction__nucleo_8py.html#a0e89ee37c633f055356f50de27558312',1,'reaction_nucleo.delay()']]],
  ['delta_42',['delta',['../classencoderDriver_1_1EncoderDriver.html#accdf9df8f9751b4af3285840ddb95155',1,'encoderDriver::EncoderDriver']]],
  ['deriv_43',['deriv',['../classgeneric__closedloop_1_1closedloop.html#ac4659b8ad6e00c33a5fc6b95584cea21',1,'generic_closedloop::closedloop']]],
  ['disable_44',['disable',['../classmotorDriver_1_1MotorDriver.html#a6a9a48a1183181eae79c5478efb89641',1,'motorDriver::MotorDriver']]],
  ['dist_45',['dist',['../classtouch__driver_1_1touch__driver.html#a1918ef884fce8c962ef336eddd0ef236',1,'touch_driver::touch_driver']]],
  ['duty_46',['duty',['../motorDriver_8py.html#a3369674d93ab821fe5dd9b0ff654511d',1,'motorDriver']]],
  ['dx_47',['dx',['../classtouch__driver_1_1touch__driver.html#aa53214f57d79c2ccbce5c54be271907f',1,'touch_driver::touch_driver']]],
  ['dy_48',['dy',['../classtouch__driver_1_1touch__driver.html#aa248d47c5206d412b04dd5ee8606664c',1,'touch_driver::touch_driver']]]
];
