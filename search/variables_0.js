var searchData=
[
  ['a_287',['a',['../temp__getter_8py.html#a892eb9e0ffa97bfdc98c867528cbc12e',1,'temp_getter']]],
  ['act_288',['act',['../classgeneric__closedloop_1_1closedloop.html#a3f7b7a195be881405c36ddac8ddc3714',1,'generic_closedloop::closedloop']]],
  ['adc_289',['ADC',['../ADC__backend_8py.html#a2164e70c51ebc203ed959bf6fbe0bf56',1,'ADC_backend']]],
  ['adc_5fx_290',['ADC_x',['../classRTP__Driver_1_1RTP__Driver.html#a1de2a570f52afe68f31e248430d11b74',1,'RTP_Driver.RTP_Driver.ADC_x()'],['../classRTP__Driver__Calibrated_1_1RTP__Driver.html#a55bdaf10a530b876ae80ffea636a06f6',1,'RTP_Driver_Calibrated.RTP_Driver.ADC_x()']]],
  ['adc_5fy_291',['ADC_y',['../classRTP__Driver_1_1RTP__Driver.html#a73200575474c4fcb392b2fe329c7e1f6',1,'RTP_Driver.RTP_Driver.ADC_y()'],['../classRTP__Driver__Calibrated_1_1RTP__Driver.html#a5fadb4834ee2c375dc90188ceec1b80b',1,'RTP_Driver_Calibrated.RTP_Driver.ADC_y()']]],
  ['addr_292',['addr',['../classmcp9808_1_1I2C__driver.html#a49949acaf983efe93350b0bf8771d726',1,'mcp9808::I2C_driver']]],
  ['amb_5ftemp_293',['amb_temp',['../plotter_8py.html#ac6c085efa648bd2e58a7a5a29433a627',1,'plotter.amb_temp()'],['../temp__getter_8py.html#a82c84efcfeb35d4854833d57bf0969f6',1,'temp_getter.amb_temp()']]],
  ['avg_5freact_294',['avg_react',['../reaction_8py.html#afbcc382a6f5fe8ef0a31ce91250c7968',1,'reaction.avg_react()'],['../reaction__nucleo_8py.html#a50673c4b27e23fb0475f7a19fadb2284',1,'reaction_nucleo.avg_react()']]]
];
