var annotated_dup =
[
    [ "bno055", null, [
      [ "BNO055", "classbno055_1_1BNO055.html", "classbno055_1_1BNO055" ]
    ] ],
    [ "bno055_base", null, [
      [ "BNO055_BASE", "classbno055__base_1_1BNO055__BASE.html", "classbno055__base_1_1BNO055__BASE" ]
    ] ],
    [ "closedLoop", null, [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "coms_driver_MCU", null, [
      [ "coms_driver", "classcoms__driver__MCU_1_1coms__driver.html", "classcoms__driver__MCU_1_1coms__driver" ]
    ] ],
    [ "coms_driver_pc", null, [
      [ "coms_driver", "classcoms__driver__pc_1_1coms__driver.html", "classcoms__driver__pc_1_1coms__driver" ]
    ] ],
    [ "encoderDriver", null, [
      [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "generic_closedloop", null, [
      [ "closedloop", "classgeneric__closedloop_1_1closedloop.html", "classgeneric__closedloop_1_1closedloop" ]
    ] ],
    [ "mcp9808", null, [
      [ "I2C_driver", "classmcp9808_1_1I2C__driver.html", "classmcp9808_1_1I2C__driver" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "RTP_Driver", null, [
      [ "RTP_Driver", "classRTP__Driver_1_1RTP__Driver.html", "classRTP__Driver_1_1RTP__Driver" ]
    ] ],
    [ "RTP_Driver_Calibrated", null, [
      [ "RTP_Driver", "classRTP__Driver__Calibrated_1_1RTP__Driver.html", "classRTP__Driver__Calibrated_1_1RTP__Driver" ]
    ] ],
    [ "taskController", null, [
      [ "TaskController", "classtaskController_1_1TaskController.html", "classtaskController_1_1TaskController" ]
    ] ],
    [ "touch_driver", null, [
      [ "touch_driver", "classtouch__driver_1_1touch__driver.html", "classtouch__driver_1_1touch__driver" ]
    ] ],
    [ "vendotron_FSM", null, [
      [ "task_vendotron", "classvendotron__FSM_1_1task__vendotron.html", "classvendotron__FSM_1_1task__vendotron" ]
    ] ]
];