var ADC__backend_8py =
[
    [ "ADC", "ADC__backend_8py.html#a2164e70c51ebc203ed959bf6fbe0bf56", null ],
    [ "button", "ADC__backend_8py.html#a500423559f508538401b797e5d0521c1", null ],
    [ "com", "ADC__backend_8py.html#a97f5f271f4dee56aa8b5467ea9519708", null ],
    [ "curr_time", "ADC__backend_8py.html#a23d4995c0865cc22ca9720c4456f701a", null ],
    [ "data", "ADC__backend_8py.html#a6c4d4b96d08792a54c1fad3d294ea10e", null ],
    [ "led", "ADC__backend_8py.html#afb141de5acc027a169f0cdbbb46fe2eb", null ],
    [ "read_time", "ADC__backend_8py.html#abd737adfbd621208c0f612df6f8b972f", null ],
    [ "S0_init", "ADC__backend_8py.html#a4a79494158a5d43ea03ca5344c4282f1", null ],
    [ "S1_wait", "ADC__backend_8py.html#a9405eb26389cf5bad969e9ee7006bd7c", null ],
    [ "S2_measure", "ADC__backend_8py.html#a2a6c2861ccb26e42655555c132e4facd", null ],
    [ "S3_send", "ADC__backend_8py.html#aa309d64850875345655380926cb6c0d3", null ],
    [ "sent", "ADC__backend_8py.html#a1760cd70c12bd8d2a5eff32bf682f309", null ],
    [ "ser", "ADC__backend_8py.html#af32bf33a88512587edaeb310a4c105e8", null ],
    [ "start_time", "ADC__backend_8py.html#a7fad833deff5943bee3b1eefbaf7a43f", null ],
    [ "state", "ADC__backend_8py.html#a0c572db37045634aff754e9c62b4d551", null ],
    [ "tim", "ADC__backend_8py.html#af0c136bf3e9df13fca9580a75d17d6b9", null ],
    [ "val", "ADC__backend_8py.html#af2e4827cdba689461d0bb4bdf3a72a44", null ]
];