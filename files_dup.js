var files_dup =
[
    [ "ADC_backend.py", "ADC__backend_8py.html", "ADC__backend_8py" ],
    [ "ADC_frontend.py", "ADC__frontend_8py.html", "ADC__frontend_8py" ],
    [ "change_getter.py", "change__getter_8py.html", "change__getter_8py" ],
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "coms_driver_MCU.py", "coms__driver__MCU_8py.html", [
      [ "coms_driver", "classcoms__driver__MCU_1_1coms__driver.html", "classcoms__driver__MCU_1_1coms__driver" ]
    ] ],
    [ "coms_driver_pc.py", "coms__driver__pc_8py.html", [
      [ "coms_driver", "classcoms__driver__pc_1_1coms__driver.html", "classcoms__driver__pc_1_1coms__driver" ]
    ] ],
    [ "encoderDriver.py", "encoderDriver_8py.html", "encoderDriver_8py" ],
    [ "generic_closedloop.py", "generic__closedloop_8py.html", [
      [ "closedloop", "classgeneric__closedloop_1_1closedloop.html", "classgeneric__closedloop_1_1closedloop" ]
    ] ],
    [ "kyb_driver.py", "kyb__driver_8py.html", "kyb__driver_8py" ],
    [ "main.py", "main_8py.html", null ],
    [ "mcp9808.py", "mcp9808_8py.html", [
      [ "I2C_driver", "classmcp9808_1_1I2C__driver.html", "classmcp9808_1_1I2C__driver" ]
    ] ],
    [ "motorDriver.py", "motorDriver_8py.html", "motorDriver_8py" ],
    [ "plotter.py", "plotter_8py.html", "plotter_8py" ],
    [ "reaction.py", "reaction_8py.html", "reaction_8py" ],
    [ "reaction_nucleo.py", "reaction__nucleo_8py.html", "reaction__nucleo_8py" ],
    [ "RTP_Driver.py", "RTP__Driver_8py.html", "RTP__Driver_8py" ],
    [ "shares_lab1.py", "shares__lab1_8py.html", "shares__lab1_8py" ],
    [ "taskController.py", "taskController_8py.html", [
      [ "TaskController", "classtaskController_1_1TaskController.html", "classtaskController_1_1TaskController" ]
    ] ],
    [ "temp_getter.py", "temp__getter_8py.html", "temp__getter_8py" ],
    [ "term_project_main.py", "term__project__main_8py.html", "term__project__main_8py" ],
    [ "touch_driver.py", "touch__driver_8py.html", [
      [ "touch_driver", "classtouch__driver_1_1touch__driver.html", "classtouch__driver_1_1touch__driver" ]
    ] ],
    [ "vendotron_FSM.py", "vendotron__FSM_8py.html", "vendotron__FSM_8py" ],
    [ "vendotron_main.py", "vendotron__main_8py.html", "vendotron__main_8py" ]
];