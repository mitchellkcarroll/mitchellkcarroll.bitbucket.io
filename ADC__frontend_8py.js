var ADC__frontend_8py =
[
    [ "on_keypress", "ADC__frontend_8py.html#a617e6c8014e410f75d755af70e65c43d", null ],
    [ "com", "ADC__frontend_8py.html#ae604942f2b045b6f1e1c4c943003d5ae", null ],
    [ "curr_time", "ADC__frontend_8py.html#ad3bb0ea5775f9687f8ce67dbb2a740a1", null ],
    [ "data", "ADC__frontend_8py.html#a8e4960651166e12d5a55d68a76520aa9", null ],
    [ "interval", "ADC__frontend_8py.html#a614812363f95ad9f8d35f9c021249a81", null ],
    [ "linestyle", "ADC__frontend_8py.html#a7b3b3e029cc10cbfd78360e29ee20d41", null ],
    [ "myval", "ADC__frontend_8py.html#add7ec115a7975484035986abaf95b87e", null ],
    [ "next_time", "ADC__frontend_8py.html#aacbd05e7e968b217e5a5cd05edf01d39", null ],
    [ "pushed_key", "ADC__frontend_8py.html#a985f714b18ff2f1afcfc3cf4776f85bd", null ],
    [ "rows", "ADC__frontend_8py.html#a0aec02fefeee6532491050266621ac2f", null ],
    [ "S0_init", "ADC__frontend_8py.html#a6c4dec85b0732fdf695b9c6cfa9b4985", null ],
    [ "S1_send", "ADC__frontend_8py.html#ab147d929c239559738f6256956ae1a85", null ],
    [ "S2_wait", "ADC__frontend_8py.html#a0ebe87dda3275272452c35991c501603", null ],
    [ "S3_plot", "ADC__frontend_8py.html#a0b5136f0a746376eae7bbd5b5f88090d", null ],
    [ "start_time", "ADC__frontend_8py.html#a30c4979c8dfcdf2f03bf3525168d728d", null ],
    [ "state", "ADC__frontend_8py.html#ababe78ecd8ffa72a0e40561c455de5b2", null ],
    [ "time", "ADC__frontend_8py.html#a09878128fb22f8d4ea8ac90f0cd67456", null ],
    [ "write", "ADC__frontend_8py.html#a5a64767a2b117edf00d915e6e6988739", null ]
];