/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Code Documentation", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Hardware", "index.html#hard", null ],
    [ "Source Code", "index.html#code", null ],
    [ "Vendotron Simulation", "sec_fsm.html", [
      [ "Overview", "sec_fsm.html#overviewH0", null ],
      [ "File List", "sec_fsm.html#fileH0", null ]
    ] ],
    [ "Reaction Time Tester", "Reaction.html", [
      [ "Overview", "Reaction.html#overviewreact", null ],
      [ "File List", "Reaction.html#filereact", null ]
    ] ],
    [ "Voltage Measurement for a Button Press", "button.html", [
      [ "Overview", "button.html#overviewbutton", null ],
      [ "File List", "button.html#filebutton", null ]
    ] ],
    [ "Temperature Data Collection", "temp.html", [
      [ "Overview", "temp.html#tempoverview", null ],
      [ "File List", "temp.html#filetemp", null ]
    ] ],
    [ "Balance Table Dynamics", "tipsy.html", [
      [ "Overview", "tipsy.html#balanceoverview", null ],
      [ "Tilt Table Kinematics", "tipsy.html#kine", null ],
      [ "Ball Kinematics", "tipsy.html#ballkine", null ],
      [ "Kinetics", "tipsy.html#kinetics", null ],
      [ "MATLAB code", "tipsy.html#code5", null ]
    ] ],
    [ "Balance Table Simulation", "lab6.html", [
      [ "Overview", "lab6.html#overview6", null ],
      [ "MATLAB code", "lab6.html#code6", null ],
      [ "Plots", "lab6.html#plot", null ]
    ] ],
    [ "Touch Panel Driver", "touch.html", [
      [ "Overview", "touch.html#overview7", null ],
      [ "File List", "touch.html#file7", null ]
    ] ],
    [ "Motor and Encoder Drivers", "ME.html", [
      [ "Overview", "ME.html#overview8", null ],
      [ "File List", "ME.html#file8", null ]
    ] ],
    [ "Balance Table", "bal.html", [
      [ "Summary", "bal.html#lab0x09_summary", null ],
      [ "Controller Tuning", "bal.html#lab0x09_controller_tuning", null ],
      [ "Challenges", "bal.html#lab0x09_challenges", null ],
      [ "Results", "bal.html#lab0x09_results", null ],
      [ "Discussion", "bal.html#lab0x09_discussion", null ],
      [ "Task Controller State-Transition Diagram", "bal.html#lab0x09_FSD", null ],
      [ "Controller Task Diagram", "bal.html#lab0x09_Task_Diagram", null ],
      [ "Video Demo", "bal.html#lab0x09_video_demo", null ],
      [ "Documentation", "bal.html#lab0x09_documentation", null ],
      [ "Source Code", "bal.html#lab0x09_source", null ],
      [ "Documentation for the IMU driver", "bal.html#bno055", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ADC__backend_8py.html",
"classtaskController_1_1TaskController.html#ab69135646daf9c62e4e5b1987719b8da"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';